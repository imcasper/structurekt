package ru.casperix.app.renderer

import ru.casperix.app.renderer.RenderConfig.ACTUAL_TILE_COLOR
import ru.casperix.app.renderer.RenderConfig.AFFECTED_TILE_COLOR
import ru.casperix.app.renderer.RenderConfig.ORIGINAL_TILE_COLOR
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.renderer.Renderer2D

class PathInfoRenderer(
    val dimension: Dimension2i,
    val affectedTiles: Collection<Vector2i>,
    val originalTiles: Collection<Vector2i>,
    val actualTiles: Collection<Vector2i>,
) {

    private val groupsRenderer = TileGroupsRenderer(
        dimension,
        listOf(
            TileGroupsRenderer.TileGroup(AFFECTED_TILE_COLOR, affectedTiles),
            TileGroupsRenderer.TileGroup(ORIGINAL_TILE_COLOR, originalTiles),
            TileGroupsRenderer.TileGroup(ACTUAL_TILE_COLOR, actualTiles),
        )
    )

    fun render(renderer: Renderer2D) {
        groupsRenderer.render(renderer)
    }
}