package ru.casperix.app.renderer

import ru.casperix.app.renderer.RenderConfig.ROAD_COLOR_1
import ru.casperix.app.renderer.RenderConfig.ROAD_COLOR_2
import ru.casperix.app.renderer.RenderConfig.ROAD_THICK
import ru.casperix.math.color.Colors
import ru.casperix.math.geometry.Line2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.Renderer2D

class FloatPathRenderer(
    val pathPoints: List<Vector2f>
) {


    fun render(renderer: Renderer2D) {
        (1 until pathPoints.size).forEachIndexed { index, id ->
            val a = pathPoints[id - 1]
            val b = pathPoints[id]
            val color = if (index % 2 == 0) ROAD_COLOR_1 else ROAD_COLOR_2
            renderer.drawLine(color, Line2f(point(a), point(b)), ROAD_THICK)
        }
    }

    private fun point(tile: Vector2f): Vector2f {
        return tile.toVector2f()
    }
}