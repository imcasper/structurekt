# Path factory

First implement the `NodeEstimator` interface. 
Then pass it to the `PathFactory` with start point. 
Finished path will be in the `PathFactory:path` field. Or null if the path is not found.
You can also see the implementation of `NodeEstimator` in `GirdBasedPath`.
Also see an `sample` project

Example of the result of searching for `floating paths` on a tile map between multiple points:
![sample.png](sample.png)

### Deploy library

#### Local
Just add water:
- `./gradlew publishToMavenLocal`

#### Nexus
Need variables (ask the administrator):
`ossrhUsername`,
`ossrhPassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.

Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin