package ru.casperix.path.custom.float_path

import ru.casperix.path.custom.float_path.TileWeightCalculator.getTileComplexity
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.path.api.NodeEstimator
import kotlin.math.max
import kotlin.math.roundToInt

class GirdBasedPath(val mapInfo: MapInfo, val mode: NeighboursMode, val start: Vector2i, val target: Vector2i) :
    NodeEstimator<Vector2i> {
    private val ftoif = 1000f

    private val area = Box2i.byDimension(Vector2i.ZERO, mapInfo.map.dimension)

    private val neighbours = when (mode) {
        NeighboursMode.ORTHOGONAL -> NEIGHBOURS_ORTHOGONAL
        NeighboursMode.DIAGONAL -> NEIGHBOURS_DIAGONAL
        NeighboursMode.EXTENDED2 -> NEIGHBOURS_EXTENDED2
        NeighboursMode.EXTENDED3 -> NEIGHBOURS_EXTENDED3
    }

    companion object {
        private val NEIGHBOURS_ORTHOGONAL = listOf(
            Vector2i(-1, 0),
            Vector2i(0, -1),
            Vector2i(0, 1),
            Vector2i(1, 0),
        )

        private val NEIGHBOURS_DIAGONAL = listOf(
            Vector2i(-1, -1),
            Vector2i(-1, 0),
            Vector2i(-1, 1),
            Vector2i(0, -1),
            Vector2i(0, 1),
            Vector2i(1, -1),
            Vector2i(1, 0),
            Vector2i(1, 1),
        )

        private val NEIGHBOURS_EXTENDED2 = NEIGHBOURS_DIAGONAL + listOf(
            Vector2i(-2, -1),
            Vector2i(-2, 1),
            Vector2i(2, -1),
            Vector2i(2, 1),
            Vector2i(-1, -2),
            Vector2i(1, -2),
            Vector2i(-1, 2),
            Vector2i(1, 2),
        )

        private val NEIGHBOURS_EXTENDED3 = NEIGHBOURS_EXTENDED2 + listOf(
            Vector2i(-3, -1),
            Vector2i(-3, 1),
            Vector2i(3, -1),
            Vector2i(3, 1),
            Vector2i(-1, -3),
            Vector2i(1, -3),
            Vector2i(-1, 3),
            Vector2i(1, 3),
        )
    }

    override fun getNeighbours(current: Vector2i): Collection<Vector2i> {
        return neighbours.map {
            it + current
        }
    }

    private fun getLengthFactor(current: Vector2i): Float {
        return current.toVector2f().length()
    }



    override fun getWeight(previous: Vector2i?, current: Vector2i, next: Vector2i): Int? {
        if (area.isOutside(current)) return null

        val dist = (current - next).toVector2f().length()
        val isDiagonal = dist > 1.4f

        val tStart = getTileComplexity(mapInfo, current)
        val tFinish = getTileComplexity(mapInfo, next)

        val tSummary = if (isDiagonal) {
            val t1 = getTileComplexity(mapInfo, Vector2i(current.x, next.y))
            val t2 = getTileComplexity(mapInfo, Vector2i(next.x, current.y))
            val tMiddle = max(t1, t2)

            (tStart + tMiddle + tFinish) / 3f
        } else {
            (tStart + tFinish) / 2f
        }

        if (!tSummary.isFinite()) {
            return null
        }

        return (ftoif * tSummary * dist).roundToInt()
    }

    override fun getHeuristic(current: Vector2i): Int {
        return (ftoif * getLengthFactor(current - target)).roundToInt()
    }

    override fun isFinish(previous: Vector2i?, current: Vector2i): Boolean {
        return current == target
    }
}