package ru.casperix.path.custom.float_path

import ru.casperix.math.array.ArrayAccessND
import ru.casperix.math.array.CustomMap2D
import ru.casperix.math.function.random2dFunctions
import ru.casperix.math.perlin.ValueNoise2D
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector2i
import kotlin.math.max

class SurfaceMapBuilder(override val dimension: Vector2i, val type: SurfaceGeneratorType, val randomRange: Float? = null) : SurfaceMap {
    private val noise = ValueNoise2D(scale = Vector3d(1.0 / 16.0))

    private val map = CustomMap2D<Tile>(dimension, MutableList(dimension.volume()) {
        val position = ArrayAccessND.position2D(dimension, it)
        generateTile(position)
    })

    private fun generateTile(position: Vector2i): Tile {
        if (type == SurfaceGeneratorType.FLAT) {
            return Tile.GRASS
        }

        val randOffset = if (randomRange == null) {
            0f
        } else {
            val randFactor = random2dFunctions[0](position.x, position.y).toFloat()
            (randFactor * 0.5f + 0.5f) * randomRange
        }

        val f1 = noise.output(position.x.toDouble(), position.y.toDouble() + 1e6).toFloat() * 16f
        val f2 = noise.output(position.x.toDouble() + 1e6, position.y.toDouble()).toFloat() * 16f
        if (f1 < -0.5f) return Tile.WATER

        return if (f2 < 0.5f) {
            Tile(max(Tile.GRASS.complexity + randOffset, 0f))
        } else {
            Tile(max(Tile.ROCK.complexity + randOffset, 0f))
        }
    }

    override fun get(position: Vector2i): Tile {
        if (isOutside(position)) return Tile.WATER
        return map.get(position)
    }


}