package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.straight_line.float32.LineSegment2f

data class Edge(val connection: LineSegment2f) {
    val direction: DegreeFloat get() = DegreeFloat.Companion.byDirection(connection.delta())

    override fun equals(other: Any?): Boolean {
        val o = other as? Edge ?: return false
        return o.connection == connection || o.connection == connection.invert()
    }

    override fun hashCode(): Int {
        return connection.start.hashCode() + connection.finish.hashCode()
    }
}