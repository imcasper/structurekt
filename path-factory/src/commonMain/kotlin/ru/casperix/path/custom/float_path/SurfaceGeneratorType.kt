package ru.casperix.path.custom.float_path

enum class SurfaceGeneratorType {
    FLAT,
    NOISE,
}