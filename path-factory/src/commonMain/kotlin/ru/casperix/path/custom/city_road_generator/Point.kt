package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.vector.float32.Vector2f

data class Point<Marker:Any>(val position: Vector2f, val marker:Marker, val connections:Set<Vector2f>) {
    fun addConnection(other:Vector2f): Point<Marker> {
        return copy(connections = connections + other)
    }
}