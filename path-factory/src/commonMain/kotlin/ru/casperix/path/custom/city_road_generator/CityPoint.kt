package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.vector.float32.Vector2f

data class CityPoint(val pivot: Vector2f, val growSteps:Int)