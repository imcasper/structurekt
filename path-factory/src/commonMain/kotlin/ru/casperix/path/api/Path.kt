package ru.casperix.path.api

import kotlinx.serialization.Serializable


@Serializable
class Path<Node>(val nodes: List<Node>, val weight: Int) {
	val start: Node
		get() {
			return nodes.first()
		}
	val finish: Node
		get() {
			return nodes.last()
		}
}