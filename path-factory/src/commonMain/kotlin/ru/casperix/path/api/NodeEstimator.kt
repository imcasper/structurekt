package ru.casperix.path.api

interface NodeEstimator<Node> {
    fun getNeighbours(current:Node): Collection<Node>
    fun getWeight(previous:Node?, current:Node, next:Node):Int?
    fun getHeuristic(current:Node):Int
    fun isFinish(previous:Node?, current:Node):Boolean
}