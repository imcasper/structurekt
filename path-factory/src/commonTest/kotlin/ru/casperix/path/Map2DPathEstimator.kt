package ru.casperix.path

import ru.casperix.math.array.Map2D
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.path.api.NodeEstimator

class Map2DPathEstimator(val map: Map2D<Int>, val finish: Vector2i) : NodeEstimator<Vector2i> {
	override fun getNeighbours(it: Vector2i): Collection<Vector2i> {
		val v1 = it + Vector2i(-1, -1)
		val v2 = it + Vector2i(-1, 0)
		val v3 = it + Vector2i(-1, 1)
		val v4 = it + Vector2i(0, -1)

		val v5 = it + Vector2i(0, 1)
		val v6 = it + Vector2i(1, -1)
		val v7 = it + Vector2i(1, 0)
		val v8 = it + Vector2i(1, 1)
		val items = mutableListOf<Vector2i>()
		if (map.isInside(v1)) items.add(v1)
		if (map.isInside(v2)) items.add(v2)
		if (map.isInside(v3)) items.add(v3)
		if (map.isInside(v4)) items.add(v4)
		if (map.isInside(v5)) items.add(v5)
		if (map.isInside(v6)) items.add(v6)
		if (map.isInside(v7)) items.add(v7)
		if (map.isInside(v8)) items.add(v8)
		return items
	}

	override fun getWeight(previous: Vector2i?, current: Vector2i, next: Vector2i): Int? {
		return if (map.get(current) != 0 || map.get(next) != 0) null
		else if (current.x == next.x || current.y == next.y) 2 else 3
	}

	override fun getHeuristic(current: Vector2i): Int {
		return (current - finish).length() * 10
	}

	override fun isFinish(previous: Vector2i?, current: Vector2i): Boolean {
		return current == finish
	}

}